#!/bin/bash

echo "***STARTING APPLICATION CONTAINER***"

docker run -ti \
    -d \
   --rm \
    -v postgresql-data:/var/lib/postgresql/data/ \
    -p 5430:5432 \
    -e 'POSTGRES_PASSWORD=passwd postgres_user={{ project_name }}'
    postgres 
    >> /dev/null

sleep 20

echo "***STARTING DJANGO***"

python manage.py runserver 4040
