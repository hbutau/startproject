# startproject

A Template for starting django projects

This project comes with an customized User models with token authentication and registration. You can
use it to bootstrap django projects. It includes Docker, Django-Suit among other goodies. 

# Usage

```bash
$ django-admin startproject \
--extension=py,ini \
--template=https://gitlab.com/hbutau/startproject/-/archive/master/startproject-main.zip example_project
```
