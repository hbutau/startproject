FROM python:3.12.2-slim-bookworm as builder

ENV PIP_DISABLE_PIP_VERSION_CHECK 1
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONPATH /srv
ENV PYTHONUNBUFFERED 1

COPY requirements.txt /tmp/requirements.txt
RUN pip wheel --no-cache-dir --no-deps --wheel-dir /app/wheels -r
requirements.txt

# Create a group and user to run our app
ARG APP_USER=appuser

RUN groupadd -r ${APP_USER} && useradd --no-log-init -r -g ${APP_USER} ${APP_USER}

################################
#    Final Stage               #
################################

FROM python:3.12.2-slim-bookworm AS release

WORKDIR /src/


copy --from=builder /app/wheels /wheels
copy --from=builder /app/requirements.txt .

RUN pip install --no-cache /wheels/*

COPY . /src/

# Change to a non root user
user ${APP_USER}:${APP_USER}

CMD ["python", "manage.py" ,"runserver", "0.0.0.0:4000"] 
